# Register User Fonts   |    [ ![Download](https://api.bintray.com/packages/xeijin-dev/werk/register-user-fonts/images/download.svg) ](https://bintray.com/xeijin-dev/werk/register-user-fonts/_latestVersion#files)

#### (un)register fonts without <img src="https://gitlab.com/xeijin-dev/register-user-fonts/raw/master/icon.svg"  width="256" height="256">  administrator privileges on windows 

# Usage

### NAME
    register-user-fonts (ruf.exe)

### SYNOPSIS
    Register or Unregister fonts without administrator privileges on Windows.

### SYNTAX
    ruf [-Path <Object>] [-UnRegister] [<CommonParameters>]

    ruf [-Path <Object>] -CurrentDir [-UnRegister] [<CommonParameters>]

### DESCRIPTION
    Uses PowerShell & Windows APIs to recursively (un)register all font files (*.ttf, *.otf) in a folder.
    Note the effect is only temporary - the command will need to be re-run each time you log-off or restart.

### PARAMETERS
    -Path <Object>
        The path to a directory of desired fonts. The operation is recursive.
        
    -CurrentDir [<SwitchParameter>]
        Apply to fonts in the current directory. The operation is recursive.
        
    -UnRegister [<SwitchParameter>]
        Unregister fonts that have already been registered.
        
    <CommonParameters>
        This cmdlet supports the common parameters: Verbose, Debug,
        ErrorAction, ErrorVariable, WarningAction, WarningVariable,
        OutBuffer, PipelineVariable, and OutVariable. For more information, see 
        about_CommonParameters (https:/go.microsoft.com/fwlink/?LinkID=113216). 

### EXAMPLES

``` powershell
    -------------------------- EXAMPLE 1 --------------------------
    
    C:\> ruf.exe
    
    Prompts the user for PATH, then registers all *.ttf & *.otf fonts found in PATH and any sub-folders.
    If the user supplies a blank input the current folder will be used.
    
    -------------------------- EXAMPLE 2 --------------------------
    
    C:\> ruf.exe "C:/MyCustomFonts"
    
    C:\> ruf.exe -Path "C:/MyCustomFonts"
    
    Registers all *.ttf & *.otf fonts found in "C:/MyCustomFonts" and any sub-folders.
    
    -------------------------- EXAMPLE 3 --------------------------
    
    C:\MyCustomFonts> ruf.exe -CurrentDir
    
    Registers all *.ttf & *.otf fonts found in the current folder ("C:/MyCustomFonts") and any sub-folders.
    
    -------------------------- EXAMPLE 4 --------------------------
    
    C:\> ruf.exe "C:/MyCustomFonts" -UnRegister
    
    C:\MyCustomFonts> ruf.exe -CurrentDir -UnRegister
    
    Unregisters all *.ttf & *.otf fonts found in "C:/MyCustomFonts" and any sub-folders.    
```

# Credit

- original code: 'Parbu' from: https://superuser.com/a/1306464
- icon: https://www.freepik.com/home