<#
.SYNOPSIS
    Register or Unregister fonts without administrator privileges on Windows.
.DESCRIPTION
    Uses PowerShell & Windows APIs to recursively (un)register all font files (*.ttf, *.otf) in a folder.
    Note the effect is only temporary - will need to run the program each time you log-off or reboot.
.PARAMETER Path
    The path to a directory of desired fonts. The operation is recursive.
.PARAMETER CurrentDir
    Apply to fonts in the current directory. The operation is recursive.
.PARAMETER UnRegister
    Unregister fonts that have already been registered.
.EXAMPLE
    C:\> ruf.exe

    Prompts the user for PATH, then registers all *.ttf & *.otf fonts found in PATH and any sub-folders.
    If the user supplies a blank input the current folder will be used.
.EXAMPLE
    C:\> ruf.exe "C:/MyCustomFonts"
    C:\> ruf.exe -Path "C:/MyCustomFonts"

    Registers all *.ttf & *.otf fonts found in "C:/MyCustomFonts" and any sub-folders.
.EXAMPLE
    C:\MyCustomFonts> ruf.exe -CurrentDir

    Registers all *.ttf & *.otf fonts found in the current folder ("C:/MyCustomFonts") and any sub-folders.
.EXAMPLE
    C:\> ruf.exe "C:/MyCustomFonts" -UnRegister
    C:\MyCustomFonts> ruf.exe -CurrentDir -UnRegister

    Unregisters all *.ttf & *.otf fonts found in "C:/MyCustomFonts" and any sub-folders.
.NOTES
    Author: xeijin
    Date:   August 03, 2019    
#>

# Code adapted by xeijin (www.xeijin.org) from Pabru's answer on SuperUser: https://superuser.com/a/1306464
# Icon created by: https://www.freepik.com/home

[CmdletBinding(DefaultParameterSetName = 'WithPath')]
param(

    # Message (aka string) is mandatory regardless of parameter set
    [Parameter(ParameterSetName = 'WithPath', Mandatory = $false)]
    [Parameter(ParameterSetName = 'NoPath', Mandatory = $false)]
    [Parameter(Position = 0)]
    [Object]$Path,

    # error has pre-set colours, no other options
    [Parameter(ParameterSetName = 'NoPath', Mandatory = $true)]
    [switch]$CurrentDir,
    
    [Parameter(ParameterSetName = 'WithPath', Mandatory = $false)]
    [Parameter(ParameterSetName = 'NoPath', Mandatory = $false)]
    [switch]$UnRegister
)

# Add new type for 'AddFontResource'
Add-Type -Name FontAdd -Namespace "" -Member @"
[DllImport("gdi32.dll")]
public static extern int AddFontResource(string filePath);
"@

# Add new type for 'RemoveFontResource'
Add-Type -Name FontRemove -Namespace "" -Member @"
[DllImport("gdi32.dll")]
public static extern int RemoveFontResource(string filePath);
"@

    if ($CurrentDir) {$Path = Get-Location}
    if (-not $Path) {$Path = Read-Host -Prompt "Path to folder with *.ttf and *.otf fonts to register (note: operation is recursive)"}

    try {
        if (-not (Test-Path -Path $Path)) {
            Write-Output "`nThe path '$Path' is not valid.`n"
            exit
        }
    }

    catch [System.Management.Automation.ParameterBindingException] {
        Write-Output "No path was provided. Using current directory."
        $Path = Get-Location
        }

    if (Test-Path -Path $Path) {
    Write-Output "Looking for fonts in:`n '$Path'"
    $folder = Get-ChildItem -Path $Path -Recurse -Include *.ttf, *.otf
    
    if (-not $folder) {Write-Output "No font files were found in '$Path'."; exit}

    foreach($font in $folder) {

        $fontName = $font.Name
        
        if ($UnRegister) {
            if ([FontRemove]::RemoveFontResource($font.FullName) -eq 1) {
                Write-Output "Removed $fontName."
            }
        }
        
        if (-not $UnRegister) {
            if ([FontAdd]::AddFontResource($font.FullName) -eq 1) {
                Write-Output "Added $fontName."
            }
        }

    }

    Write-Output "`n Operation complete. `n"

    }

    else {Write-Output "An unknown error occurred."; throw}